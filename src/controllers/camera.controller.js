const camera = async (req, res) => {
  const { id } = req.params;
  try {
    if (id == 1) {
      res.json({
        uri: "http://172.16.1.18/ISAPI/streaming/channels/102/httpPreview",
      });
    }
    if (id == 2) {
      res.json({
        uri: "http://172.16.1.18/ISAPI/streaming/channels/202/httpPreview",
      });
    }
    if (id == 3) {
      res.json({
        uri: "http://156.98.0.58/mjpg/video.mjpg",
      });
    }
  } catch (err) {
    res.json({ error: err.message });
  }
};

module.exports = camera;
