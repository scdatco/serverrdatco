const axios = require("axios");

//const id = "4D9C44";
//82733E
//827548

//const id = "82733E";
const deviseGet = async (req, res) => {
  hd = req.headers.hd;
  try {
    const { data } = await axios.get(
      `https://api.sigfox.com/v2/devices/${hd}/messages`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${process.env.DEVICES_AUTH_REP}`,
        },
      }
    );
    //console.log(data);
    res.json(data);
  } catch (err) {
    console.error(err);
  }
};

const locationGet = async (req, res) => {
  hd = req.headers.hd;
  try {
    const { data } = await axios.get(
      `https://api.sigfox.com/v2/devices/${hd}/locations`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${process.env.DEVICES_AUTH_REP}`,
        },
      }
    );
    //console.log(data);
    res.json(data);
  } catch (err) {
    console.error(err);
  }
};

module.exports = { deviseGet, locationGet };
