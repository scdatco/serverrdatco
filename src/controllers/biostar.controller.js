process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const fs = require("fs");
const axios = require("axios");
const getToken = require("../services/gettoken");

let schema = {
  Query: {
    limit: 51,
    conditions: [
      {
        column: "device_id.id",
        operator: 2,
        values: ["939263514", "939263515"],
      },
      {
        column: "datetime",
        operator: 3,
        values: ["2021-04-16T:00:00.000Z", new Date()],
      },
      {
        column: "event_type_id.code",
        operator: 0,
        values: ["4102"],
      },
    ],
  },
};

const register = async (req, res) => {
  const body = JSON.stringify(schema);
  getToken();
  const id = fs.readFileSync("./storage/id.txt", "utf8", function (e) {
    if (e) throw e;
    console.log("Create id");
    return;
  });

  const headers = {
    "Content-Type": "application/json",
    "bs-session-id": id,
  };

  try {
    const { data } = await axios.post(
      "https://172.16.1.17/api/events/search",
      body,
      {
        headers: headers,
      }
    );
    res.json(data.EventCollection);
  } catch (err) {
    console.error(err);
  }
};

module.exports = register;
