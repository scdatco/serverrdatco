const routes = require("express").Router();
const camera = require("../controllers/camera.controller");

routes.get("/:id", camera);

module.exports = routes;
