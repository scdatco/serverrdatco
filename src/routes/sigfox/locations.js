const routes = require("express").Router();
const { locationGet } = require("../../controllers/devices.controllers");

routes.get("/", locationGet);

module.exports = routes;
