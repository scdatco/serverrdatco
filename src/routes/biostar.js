const routes = require("express").Router();
const register = require("../controllers/biostar.controller");

routes.get("/", register);

module.exports = routes;
