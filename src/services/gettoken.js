process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
require("dotenv").config();
const fs = require("fs");
const path = require("path");
const axios = require("axios");

const user = process.env.USERBS;
const password = process.env.PASSBS;

const getcred = { login_id: user, password: password };

const createId = (id) => {
  fs.writeFile(path.join(__dirname, "../storage/id.txt"), id, (e) => {
    if (e) throw e;
    console.log("Create id");
  });
};

const getToken = () => {
  let data = JSON.stringify({
    User: getcred,
  });

  let config = {
    method: "post",
    url: "https://172.16.1.17/api/login",
    headers: {
      accept: "application/json",
      "Content-Type": "application/json",
    },
    data: data,
  };

  axios(config)
    .then(function (response) {
      const header = response.headers;
      const bsid = header["bs-session-id"];
      createId(bsid);
    })
    .catch(function (error) {
      console.log(error);
    });
};

module.exports = getToken;
