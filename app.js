const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const cors = require("cors");
const fs = require("fs");
const indexRoute = require("./src/routes/index");
const userRoute = require("./src/routes/users");
const itemsRoute = require("./src/routes/items");
const messageSigfox = require("./src/routes/sigfox/message");
const locationSigfox = require("./src/routes/sigfox/locations");
const weatherRoute = require("./src/routes/weather");
const cameraRes = require("./src/routes/camera.route");
const bioStar = require("./src/routes/biostar");
const dotenv = require("dotenv").config();
const http = require("http");
const sequelize = require("./src/loaders/sequelize");
const getToken = require("./src/services/gettoken");

//UTILIZACIÓN DE VARIABLE DE ENTORNO

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
//RESPUESTA DE LOGUEO
app.use(logger("dev"));
//CORS
app.use(cors());

app.use("/", indexRoute);
app.use("/users", userRoute);
app.use("/items", itemsRoute);
app.use("/devices", messageSigfox);
app.use("/locations", locationSigfox);
app.use("/weather", weatherRoute);
app.use("/camera", cameraRes);
app.use("/biostar", bioStar);

//console.log(global);

const server = http.createServer(app);
const port = process.env.PORT || "5000";

server.listen(port, () => {
  try {
    console.log(`Enable port in localhost:${port}`);
    sequelize.sync().then(() => {
      console.log("Connection has been established successfully.");
    });
  } catch (e) {
    console.log(e);
  }
});
