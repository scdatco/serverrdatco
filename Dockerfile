FROM node:14.16.1

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY app.js ./app.js
COPY src ./src

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

ENV PORT 8000
ENV API_W_KE ea89c702f92b45d3b5e1b8d235c9d33b
ENV USERDB sa
ENV PASSDB Password01

EXPOSE 8080

CMD [ "node", "app.js" ]